/* $Id: keepalive.h,v 1.1 2006/02/05 15:19:12 jirka Exp $ */
/*
 * netrw tools.
 * Copyright (C) 2006 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/** @file
 * netrw tools.
 * @author Jiri Denemark
 * @date 2006
 * @version $Revision: 1.1 $ $Date: 2006/02/05 15:19:12 $
 */

#ifndef KEEPALIVE_H
#define KEEPALIVE_H

#include "proto_types.h"

#define PAR_STR_KEEPALIVE   "keepalive"

#define KEEPALIVE_OFF       0
#define KEEPALIVE_ON        1


/** Check keep-alive connection parameters.
 *
 * Check whether all keep-alive connection parameters are properly defined
 * and known to both of the parties.
 *
 * @param req_params
 *      parameters requested by remote party.
 *
 * @param params
 *      locally requested parameters.
 *
 * @return
 *      nothing.
 */
extern void keepalive_proto_check(struct proto_params *req_params,
                                  struct proto_params *params);


/** Send keep-alive packet if needed. */
extern void keepalive_write(void);

/** Send end-of-keep-alive-packets mark. */
extern void keepalive_write_finish(void);

/** Receive keep-alive packet if any. */
extern void keepalive_read(void);

/** Receive all remaining keep-alive packets. */
extern void keepalive_read_finish(void);

#endif

