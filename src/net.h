/* $Id: net.h,v 1.7 2006/02/05 15:19:12 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef NET_H
#define NET_H

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>


#if !HAVE_SOCKLEN_T
#define socklen_t int
#endif


enum ip_mode {
    IM_ANY,
    IM_HOST
};

enum connection_mode {
    CM_READER,
    CM_READER_FW,
    CM_WRITER,
    CM_WRITER_FW
};

enum transport_protocol {
    TP_UDP,
    TP_TCP
};

enum send_mode {
    SM_LATER,
    SM_IMMEDIATELY
};


/*****************************************************************************
 * host, transport protocol, and service
 */

/*
 * set_host(host)
 *
 * resolve an IP address assigned to the specified host domain name
 *
 * return value:
 *      FN_SUCCESS ... IP address successfully resolved
 *      FN_FAILURE ... unknown host
 *
 * side effects:
 *      sets static variable ip to the IP address
 */
int set_host(char *host);

/*
 * set_service(service, tp)
 * 
 * resolve a port number assigned to the specified service
 *
 * return value:
 *      FN_SUCCESS ... port number successfully resolved
 *      FN_FAILURE ... unknown service
 *
 * side effects:
 *     sets static variable port according to the service 
 */
extern int set_service(char *service, enum transport_protocol tp);

/*
 * in_addr(sin, mode)
 *
 * fill in the sin structure according to the host address and the port number
 * specified by calling set_host(), and set_service()
 * an IP address within this strcture is set to
 *      host ip address iff mode == IM_HOST
 *      INADDR_ANY      iff mode == IM_ANY
 *
 * return value:
 *      FN_SUCCESS ... ok
 *      FN_FAILURE ... unknown port or invalid sin strcture
 *
 * side effects:
 *      none
 */
extern int in_addr(struct sockaddr_in *sin, enum ip_mode mode);


/*****************************************************************************
 * connection, and socket
 */

/*
 * create_data_socket(mode)
 *
 * create data socket with respect to the specified mode and the transport
 * protocol given to the set_service() function
 *
 * return value:
 *      FN_SUCCESS ... socket successfully created
 *      FN_FAILURE ... cannot create socket
 *
 * side effects:
 *      sets static variable s_data (and s_listen when it is needed)
 */
extern int create_data_socket(enum connection_mode mode);

/*
 * close_data_socket()
 *
 * close data socket
 *
 * return value:
 *      FN_SUCCESS
 *      FN_FAILURE
 *
 * side effects:
 *      sets static variable s_data to -1
 */
extern int close_data_socket(void);

/*
 * have_data_socket()
 * 
 * check if data socket exists
 *
 * return value:
 *      nonzero ... yes
 *      zero    ... no
 *
 * side effects:
 *      none
 */
extern int have_data_socket(void);

/*
 * create_control_socket()
 *
 * create control socket for the connection established by the
 * create_data_socket() function
 * !! this function MUST be called AFTER a previous successful call of
 * !! create_data_socket()
 *
 * return value:
 *      FN_SUCCESS ... socket successfully created
 *      FN_FAILURE ... cannot create socket (should not be fatal)
 *
 * side effects:
 *      sets static variable s_control
 *      allocates IO buffers
 */
extern int create_control_socket(void);

/*
 * close_control_socket()
 *
 * close control socket
 *
 * return value:
 *      FN_SUCCESS
 *      FN_FAILURE
 *
 * side effects:
 *      sets static variable s_control to -1
 *      frees IO buffers
 */
extern int close_control_socket(void);

/*
 * have_control_socket()
 * 
 * check if control socket exists
 *
 * return value:
 *      nonzero ... yes
 *      zero    ... no
 *
 * side effects:
 *      none
 */
extern int have_control_socket(void);


/*****************************************************************************
 * data connection IO
 */

/*
 * data_wait()
 *
 * wait for data on data socket
 *
 * return value:
 *      FN_SUCCESS ... data ready
 *      FN_FAILURE
 *
 * side effects:
 *      none
 */
extern int data_wait(void);

/*
 * data_read(buf, count, from, fromlen)
 *
 * read data from data socket to the specified buffer buf using either
 * recvfrom() (for UDP) or read() (for TCP)
 *
 * return value:
 *      the same as recvfrom() / read()
 *
 * side effects:
 *      none
 */
extern ssize_t data_read(void *buf, size_t count,
                         struct sockaddr_in *from, socklen_t *fromlen);

/*
 * data_write(buf, count, to, tolen)
 *
 * write data from the specified buffer buf to data socket using either
 * sendto() (for UDP) or write() (for TCP)
 *
 * return value:
 *      the same as sendto() / write()
 *
 * side effects:
 *      none
 */
extern ssize_t data_write(const void *buf, size_t count);


/*****************************************************************************
 * control connection IO
 */

/** Check whether any data can be read from control connection.
 *
 * @return
 *      @c FN_SUCCESS if data can be read by control_read()
 *      @c FN_FAILURE otherwise
 */
extern int control_check_read(void);

/*
 * control_read()
 *
 * read one line from the control connection
 * both CR+LF and LF end of the line are supported
 * lines longer than BUFFSIZE including newline character(s) are ignored
 *
 * return value:
 *      pointer to the line read (don't free it) on success
 *      NULL on failure
 *
 * side effects:
 *      static buffres r_buf and r_tmp are affected
 */
char *control_read(void);

/*
 * control_write(mode, line)
 *
 * write the specified line to the control connection
 * newline character (LF) is added automaticaly to the end of line
 * by passing SM_IMMEDIATELY to the argument mode you can assure that the line
 * is written immediately (otherwise it is written when the buffer gets full)
 *
 * return value:
 *      FN_SUCCESS
 *      FN_FAILURE
 *
 * side effects:
 *      static buffres w_buf and w_tmp are affected
 */
int control_write(enum send_mode mode, char *line);

/*
 * control_writef(mode, fmt, ...)
 *
 * format the line according the format fmt and send it by control_write()
 * format string and its parameters have exactly the same meaning as for
 * the *printf() functions
 *
 * return value:
 *      the same as control_write() returns
 *
 * side effects:
 *      the same as control_write() has
 */
int control_writef(enum send_mode mode, char *fmt, ...);

#endif

