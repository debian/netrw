/* $Id: checksum_proto.h,v 1.3 2006/02/05 15:19:12 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef CHECKSUM_PROTO_H
#define CHECKSUM_PROTO_H

#include "proto_types.h"


/*
 * checksum_proto_param_options(options, req_params, params)
 *
 * parse checksum parameter options and select the most appropriate one
 * req_params ... connection parameters requested by remote party
 * params ....... localy required connection parameters
 *
 * return value:
 *      none
 *
 * side effects:
 *      none
 */
extern void checksum_proto_param_options(char *options,
                                         struct proto_params *req_params,
                                         struct proto_params *params);

/*
 * checksum_proto_check(req_params, params)
 *
 * check whether all of the requested connection parameters are properly
 * defined and known to both of the parties
 *
 * return value:
 *      none
 *
 * side effects:
 *      none
 */
extern void checksum_proto_check(struct proto_params *req_params,
                                 struct proto_params *params);

/*
 * checksum_proto_accept_option(option, req_params, params)
 *
 * parse checksum option accepted by remote party and check if it is ok
 * req_params ... connection parameters accepted by remote party
 * params ....... parameters resultiong from the negotiation
 *
 * return value:
 *      FN_SUCCESS ... accepted option fits our needs
 *      FN_FAILURE ... we cannot accept this option
 *
 * side effects:
 *      none
 */
extern int checksum_proto_accept_option(char *option,
                                        struct proto_params *req_params,
                                        struct proto_params *params);

#endif

