/* $Id: checksum.c,v 1.17 2006/08/15 14:34:20 jirka Exp $ */

/*
 * netrw tools
 * Copyright (C) 2002 Jiri Denemark
 *
 * This file is part of netrw.
 *
 * netrw is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#if STDC_HEADERS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#endif

#include "common.h"
#include "proto_types.h"
#include "proto.h"
#include "net.h"
#include "checksum.h"
#include "checksum_proto.h"


#define MAX_LEN 1023
static int type = CHECKSUM_NONE;


static char HEXDIGIT[16] = "0123456789abcdef";

static void checksum_hash_to_string(unsigned char *hash,
                                    char *checksum,
                                    int len)
{
    const unsigned char *src;
    const unsigned char *end;
    char *dst;

    end = hash + len;
    for (src = hash, dst = checksum; src != end; src++) {
        *(dst++) = HEXDIGIT[*src / 16];
        *(dst++) = HEXDIGIT[*src % 16];
    }
    *dst = '\0';
}


#if USE_CHECKSUM_MHASH
#include <mhash.h>

#define SUM_LEN (mhash_get_block_size(checksum_mhash_hashid()) * 2)

static MHASH ctx;
static char checksum[MAX_LEN + 1];
static char remote_sum[MAX_LEN + 1];

static hashid checksum_mhash_hashid(void)
{
    switch (type) {
    case CHECKSUM_SHA1:     return MHASH_SHA1;
    case CHECKSUM_SHA256:   return MHASH_SHA256;
    case CHECKSUM_RMD160:   return MHASH_RIPEMD160;
    case CHECKSUM_MD5:      return MHASH_MD5;
    case CHECKSUM_TIGER:    return MHASH_TIGER;
    case CHECKSUM_GOST:     return MHASH_GOST;
    }

    /* unreachable */
    return (hashid) -1;
}

static void checksum_mhash_string(void *hash, char *checksum)
{
    int len;

    len = mhash_get_block_size(checksum_mhash_hashid());
    if (len * 2 > MAX_LEN)
        print(ERROR, "Checksum too long: %d\n", len * 2);

    checksum_hash_to_string((unsigned char *) hash, checksum, len);

    free(hash);
}

/* if USE_CHECKSUM_MHASH */
#elif USE_CHECKSUM_OPENSSL
#include <openssl/evp.h>

#define SUM_LEN (EVP_MD_size(checksum_ossl_type()) * 2)

static EVP_MD_CTX ctx;
static char checksum[MAX_LEN + 1];
static char remote_sum[MAX_LEN + 1];

static const EVP_MD *checksum_ossl_type(void)
{
    switch (type) {
    case CHECKSUM_SHA1:   return EVP_sha1();
    case CHECKSUM_RMD160: return EVP_ripemd160();
    case CHECKSUM_MD5:    return EVP_md5();
    }

    /* unreachable */
    return NULL;
}

static void checksum_ossl_string(char *checksum)
{
    unsigned int len;
    unsigned char hash[EVP_MAX_MD_SIZE];

    EVP_DigestFinal(&ctx, hash, &len);
    if (len * 2 > MAX_LEN)
        print(ERROR, "Checksum too long: %d\n", len * 2);

    checksum_hash_to_string(hash, checksum, len);
}

/* if USE_CHECKSUM_OPENSSL */
#elif USE_CHECKSUM_DIET
#include <md5.h>

#define SUM_LEN  (MD5_SIZE * 2)

static MD5_CTX ctx;
static char checksum[SUM_LEN + 1];
static char remote_sum[SUM_LEN + 1];

/* if USE_CHECKSUM_DIET */
#elif USE_CHECKSUM_BUILT_IN
#include "md5.h"

#define SUM_LEN (MD5_SIZE * 2)

static struct MD5Context ctx;
static char checksum[SUM_LEN + 1];
static char remote_sum[SUM_LEN + 1];

/* if USE_CHECKSUM_BUILT_IN */
#endif


void checksum_init(const struct proto_params *params)
{
    type = params->checksum;

    memset(checksum, '\0', sizeof(checksum));
    memset(remote_sum, '\0', sizeof(remote_sum));

#if USE_CHECKSUM_MHASH
    ctx = mhash_init(checksum_mhash_hashid());
#elif USE_CHECKSUM_OPENSSL
    EVP_DigestInit(&ctx, checksum_ossl_type());
#elif USE_CHECKSUM_DIET
    MD5Init(&ctx);
#elif USE_CHECKSUM_BUILT_IN
    MD5Init(&ctx);
#endif
}

void checksum_process(const void *buffer, unsigned int len)
{
#if USE_CHECKSUM_MHASH
    mhash(ctx, buffer, len);
#elif USE_CHECKSUM_OPENSSL
    EVP_DigestUpdate(&ctx, buffer, len);
#elif USE_CHECKSUM_DIET
    MD5Update(&ctx, (uint8_t *) buffer, len);
#elif USE_CHECKSUM_BUILT_IN
    MD5Update(&ctx, buffer, len);
#endif
}

char *checksum_finish(void)
{
#if USE_CHECKSUM_MHASH
    checksum_mhash_string(mhash_end(ctx), checksum);
#elif USE_CHECKSUM_OPENSSL
    checksum_ossl_string(checksum);
#elif USE_CHECKSUM_DIET
    unsigned char sig[MD5_SIZE];

    MD5Final((uint8_t *) sig, &ctx);
    checksum_hash_to_string(sig, checksum, MD5_SIZE);
#elif USE_CHECKSUM_BUILT_IN
    unsigned char sig[MD5_SIZE];

    MD5Final(&ctx, sig);
    checksum_hash_to_string(sig, checksum, MD5_SIZE);
#endif

    return checksum;
}

char *checksum_exchange(void)
{
    char header[100];
    char *line;

    if (!have_control_socket())
        return NULL;

    snprintf(header, 100, "%s(%s)", PAR_STR_CHECKSUM, CHECKSUMS[type]);

    if (control_writef(SM_IMMEDIATELY, "%s: %s",
                       header, checksum) == FN_FAILURE) {
        print(ERROR, "Cannot send %s checksum\n", CHECKSUMS[type]);
        return NULL;
    }

    if ((line = control_read()) == NULL) {
        print(ERROR, "Cannot read %s checksum\n", CHECKSUMS[type]);
        return NULL;
    }

    if (line_header(&line, header) == FN_FAILURE
        || *line != ' '
        || strlen(++line) != SUM_LEN) {
        print(ERROR, "Invalid checksum specification: ``%s''\n", line);
        return NULL;
    }

    strncpy(remote_sum, line, SUM_LEN);

    return remote_sum;
}

int checksum_validate(void)
{
    if (memcmp((void *) checksum, (void *) remote_sum, SUM_LEN) == 0)
        return FN_SUCCESS;
    else
        return FN_FAILURE;
}


void checksum_proto_param_options(char *options,
                                  struct proto_params *req_params,
                                  struct proto_params *params)
{
    int i;
    int opt = -1;
    char *option;

    /* not requested by any party, ignore it */
    if (req_params->checksum == PAR_NOT_REQ
        && params->checksum == CHECKSUM_NONE)
        return;

    /* redefined, ignore it */
    if (req_params->checksum == PAR_DEFINED
        || req_params->checksum == PAR_REDEFINED) {
        req_params->checksum = PAR_REDEFINED;
        print(WARNING, "Checksum parameter redefined; ignored\n");
        return;
    }

    while ((option = get_word(&options)) != NULL) {
        for (i = 0; i < CHECKSUMS_CNT; i++) {
            if (strmatch(option, CHECKSUMS[i]))
                break;
        }
        if (i == CHECKSUMS_CNT)
            continue;

        if (opt == -1 || params->checksum == i)
            opt = i;
    }

    if (opt > -1) {
        print(VERBOSE, "negotiated checksum algorithm: %s\n", CHECKSUMS[opt]);
        params->checksum = opt;
    }
    else {
        print(WARNING, "Your party doesn't offer any suitable "
              "checksum algorithm; checksum disabled\n");
        params->checksum = CHECKSUM_NONE;
    }

    req_params->checksum = PAR_DEFINED;
}

void checksum_proto_check(struct proto_params *req_params,
                          struct proto_params *params)
{
    if (req_params->checksum == PAR_REQUESTED) {
        print(WARNING, "Your party requires checksum but doesn't support "
              "any checksum algorithm; checksum disabled\n");
        params->checksum = CHECKSUM_NONE;
    }
    else if (req_params->checksum == PAR_NOT_REQ
             && params->checksum != CHECKSUM_NONE) {
        print(WARNING, "Your party doesn't know anything about checksum "
              "parameter you have requested; checksum disabled\n");
        params->checksum = CHECKSUM_NONE;
    }
}

int checksum_proto_accept_option(char *option,
                                 struct proto_params *req_params,
                                 struct proto_params *params)
{
    int i;

    /* redefined, ignore it */
    if (req_params->checksum == PAR_DEFINED
        || req_params->checksum == PAR_REDEFINED) {
        req_params->checksum = PAR_REDEFINED;
        print(WARNING, "Checksum parameter redefined; ignored\n");
        return FN_SUCCESS;
    }

    if (option != NULL) {
        for (i = 0; i < CHECKSUMS_CNT; i++) {
            if (strmatch(option, CHECKSUMS[i])) {
                params->checksum = i;
                break;
            }
        }
    }

    if (params->checksum == CHECKSUM_NONE) {
        print(ERROR, "Checksum algorithm negotiation failed\n");
        return FN_FAILURE;
    }
    else {
        print(VERBOSE, "negotiated checksum algorithm: %s\n",
              CHECKSUMS[params->checksum]);
    }

    return FN_SUCCESS;
}

